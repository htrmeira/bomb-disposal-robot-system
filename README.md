bomb-disposal-robot-system
==========================

Coursework for the course Real-Time and Embedded System in Coventry University 2013.1

This project simulates a system to control a robot based in this (http://www.google.co.uk/patents/US20120261204?dq=bomb+disposal+control+system&hl=en&sa=X&ei=sMMrUfWUE7LB0gWcw4HYCw&ved=0CEMQ6AEwBA).
He simulates only the most basics moviments based on the subjects presented in class.
This basics moviments are:
	* move arm: up, down, left, right, front and back.
	* move hand: open and close.
	* simulates de the sensors of the many parts of the robot.
	* simulates a battery.

between the components are:
	* on board control system
	* one hand
	* one arm
	* battery
	* log operations (two files is used for this, one for the robot operation stored in the robot hard drive other in the controller, logging controller operations)


commands:
	the commands to move arm and hand has the following formation: [component] [action] [value]
	where:
		component: arm | hand
		action: up | down | left | right | front | back | open | close
		value: is an integer that must represents the pressure, degree or distance of the moviment.

to move arm:
user@robot: arm up 20

This command will move the arm up 20cm.
The arm actions are up | down | left | right | front | back
