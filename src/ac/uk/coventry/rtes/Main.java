package ac.uk.coventry.rtes;

import ac.uk.coventry.rtes.comunication.CommunicationSystem;
import ac.uk.coventry.rtes.controler.RemoteControlSystem;
import ac.uk.coventry.rtes.controler.UserInterface;
import ac.uk.coventry.rtes.robot.OnBoardControlSystem;

public class Main {
	public static void main(String[] args) {
		CommunicationSystem communicationSystem = new CommunicationSystem();
		OnBoardControlSystem robotControlSystem = new OnBoardControlSystem(communicationSystem);
		RemoteControlSystem remoteControlSystem = new RemoteControlSystem(communicationSystem);
		
		UserInterface userIterface = new UserInterface(remoteControlSystem);
		
		userIterface.start();
		robotControlSystem.start();
		remoteControlSystem.start();
	}
}
