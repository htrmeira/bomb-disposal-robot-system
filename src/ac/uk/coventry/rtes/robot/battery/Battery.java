package ac.uk.coventry.rtes.robot.battery;

import java.text.DecimalFormat;
import java.util.Date;

import ac.uk.coventry.rtes.robot.sensor.ComponentStatus;
import ac.uk.coventry.rtes.robot.sensor.Monitorable;

/**
 * This class simulates the battery of the system.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class Battery implements Monitorable {
	public final static String BATTERY = "Battery";
	private double FULL_CHARGE = 100000; // capacity.
	private double charge = FULL_CHARGE; // current charge.
	private Date lastExecuted;
	byte status = ComponentStatus.WORKING;

	/**
	 * Decrease the this charge from the battery. Note that this method is
	 * synchronised, since many process can be decreasing charge from the
	 * battery at the same time.
	 * 
	 * @param decreasedCharge
	 *            The amount of wasted charge.
	 * @throws NoChargeException
	 *             If there is no more charge in the battery.
	 */
	public synchronized void decreaseCharge(double decreasedCharge)
			throws NoChargeException {
		if (charge - decreasedCharge <= 0) {
			charge = 0;
			status = ComponentStatus.OFF;
			throw new NoChargeException();
		} else {
			charge -= decreasedCharge;
		}
		lastExecuted = new Date();
	}

	/**
	 * Verify if there is some charge in the battery.
	 * 
	 * @return true if there is, false otherwise.
	 */
	public boolean hasCharge() {
		return charge > 0;
	}

	private double totalChargeInPercents() {
		return (charge / FULL_CHARGE) * 100;
	}

	/**
	 * Format the amount of charge in the battery.
	 * 
	 * @return the formated amount of charge of the battery.
	 */
	private String componentInfo() {
		DecimalFormat df = new DecimalFormat("#.##");
		return String.format("%s%%", df.format(totalChargeInPercents()));
	}

	@Override
	public ComponentStatus currentStatus() {
		return new ComponentStatus(lastExecuted, new Date(),
				status, componentInfo(), BATTERY);
	}

	@Override
	public String toString() {
		return componentInfo();
	}

}
