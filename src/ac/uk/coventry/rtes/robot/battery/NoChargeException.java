package ac.uk.coventry.rtes.robot.battery;

public class NoChargeException extends RuntimeException {
	private static final long serialVersionUID = -5594919763880148811L;

	public NoChargeException() {
		super("Battery has no charge");
	}
}
