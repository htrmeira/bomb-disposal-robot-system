package ac.uk.coventry.rtes.robot;

import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;

import ac.uk.coventry.rtes.comunication.Message;
import ac.uk.coventry.rtes.log.Logger;
import ac.uk.coventry.rtes.robot.battery.Battery;
import ac.uk.coventry.rtes.robot.battery.NoChargeException;
import ac.uk.coventry.rtes.robot.sensor.ComponentStatus;
import ac.uk.coventry.rtes.robot.sensor.Monitorable;

/**
 * This class represents a autonomous robot component like arm, hand, cam etc.
 *  
 *@author Heitor Meira demeloh@uni.coventry.ac.uk
 *
 */
public abstract class RobotComponent extends Thread implements Monitorable {
	private final static int BUFFER_SIZE = 5;

	private final ArrayBlockingQueue<Message> messages = new ArrayBlockingQueue<Message>(BUFFER_SIZE);
	protected final Logger logger;

	private final Battery battery;

	private Date lastExecuted;
	private byte componentStatus = ComponentStatus.IDLE;
	private boolean isOn = true;

	public RobotComponent(String componentName, Battery battery, Logger logger) {
		super(componentName);
		this.battery = battery;
		this.logger = logger;
	}

	public void run() {
		while(isOn) {
			try {
				/*
				 * waits until a message become available to be executed.
				 */
				executeCommand(messages.take());
				lastExecuted = new Date();
			} catch (InterruptedException e) {
				isOn = false;
				logger.log("[" + componentName() + "] is now offline...");
			}
		}
	}
	
	@Override
	public void start() {
		logger.log("[" + componentName() + "] starting " + componentName() + "...");
		super.start();
		logger.log("[" + componentName() + "] " +componentName() + " initialized...");
	}

	public synchronized boolean receiveMessage(Message message) {
		return messages.add(message);
	}

	public String componentName() {
		return getName();
	}

	@Override
	public ComponentStatus currentStatus() {
		componentStatus = isOn ? componentStatus : ComponentStatus.OFF;
		return new ComponentStatus(lastExecuted, new Date(), componentStatus, componentInfo(), componentName());
	}
	
	private void executeCommand(Message message) {
		componentStatus = ComponentStatus.WORKING;
		logger.log("[" + componentName() + "] executing command: " + message);
		executeComponentCommand(message);
		logger.log("[" + componentName() + "] command exected:  " + message);
		componentStatus = ComponentStatus.IDLE;
	}

	protected final void decreaseBatteryCharge(int charge) {
		try {
			battery.decreaseCharge(charge);
		} catch(NoChargeException ex) {
			logger.log("[" + componentName() + "] no battery charge.");
		}
	}

	protected final boolean batteryHasCharge() {
		return battery.hasCharge();
	}

	protected void simulateMovingTime(int movimentTime) {
		try {
			Thread.sleep(movimentTime);
		} catch (InterruptedException e) {}
	}

	protected abstract void executeComponentCommand(Message message);
	protected abstract String componentInfo();

}
