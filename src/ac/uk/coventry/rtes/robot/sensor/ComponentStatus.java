package ac.uk.coventry.rtes.robot.sensor;

import java.text.DateFormat;
import java.util.Date;

/**
 * This class represents the status of a robot component.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class ComponentStatus {
	private static final String NEVER = "NEVER";
	public static final byte ERROR = 1;
	public static final byte IDLE = 2;
	public static final byte WORKING = 3;
	public static final byte ON = 4;
	public static final byte OFF = 5;

	private final String componentName;
	private final Date lastExecuted;
	private final Date lastMonitored;
	private final byte currentStatus;
	private final String componentInformation;

	public ComponentStatus(Date lastExecuted, Date lastMonitored,
			byte currentStatus, String componentInformation,
			String componentName) {
		this.lastExecuted = lastExecuted;
		this.lastMonitored = lastMonitored;
		this.currentStatus = currentStatus;
		this.componentInformation = componentInformation;
		this.componentName = componentName;
	}

	public String componentName() {
		return componentName;
	}

	public Date lastExecuted() {
		return lastExecuted;
	}

	public Date lastMonitored() {
		return lastMonitored;
	}

	public byte currentStatus() {
		return currentStatus;
	}

	public String componentInformation() {
		return componentInformation;
	}

	public static String formatDate(Date date) {
		if (date == null) {
			return NEVER;
		} else {
			return DateFormat.getDateTimeInstance(DateFormat.SHORT,
					DateFormat.MEDIUM).format(date);
		}
	}

	public static String formatStatus(byte status) {
		switch (status) {
		case ERROR:
			return "ERROR";
		case IDLE:
			return "IDLE";
		case WORKING:
			return "WORKING";
		case ON:
			return "ON";
		case OFF:
			return "OFF";
		default:
			return "UNKNOWN";
		}
	}

	@Override
	public String toString() {
		return String.format(
				"name=%s, last-execution=(%s), last-monitored=(%s), status=%s, "
						+ "information=%s", componentName,
				formatDate(lastExecuted), formatDate(lastMonitored),
				formatStatus(currentStatus), componentInformation);
	}
}
