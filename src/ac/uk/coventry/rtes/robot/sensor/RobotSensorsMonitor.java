package ac.uk.coventry.rtes.robot.sensor;

import java.util.HashMap;
import java.util.LinkedList;

import ac.uk.coventry.rtes.comunication.Message;
import ac.uk.coventry.rtes.log.Logger;
import ac.uk.coventry.rtes.robot.OnBoardControlSystem;
import ac.uk.coventry.rtes.robot.battery.Battery;
import ac.uk.coventry.rtes.robot.battery.NoChargeException;

/**
 * This class collects represents the monitor of the status of the system. It
 * collects all the status information of the system wach 100ms and send it to
 * the controller.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class RobotSensorsMonitor extends Thread {
	private final static double SEND_MESSAGE_DISCHARGE = 2;
	private final static double SENSOR_LIFE_DISCHARGE = 0.1;

	private final LinkedList<SensorMonitor> sensors = new LinkedList<SensorMonitor>();
	private final Logger logger;

	private final OnBoardControlSystem controlSystem;
	private final Battery battery;

	private boolean isOn = true;

	public RobotSensorsMonitor(OnBoardControlSystem controlSystem, Battery battery,
			Logger logger, Monitorable... components) {
		super("RobotSensors");
		this.controlSystem = controlSystem;
		this.battery = battery;
		this.logger = logger;
		addMonitors(components);
		addComponent(battery);
		addComponent(controlSystem);
	}

	@Override
	public void run() {
		while (isOn) {
			sendSensorStatus();
			waitToSendMessage();
		}
	}

	/**
	 * Sends all the status information to the controller.
	 */
	private void sendSensorStatus() {
		try {
			controlSystem.sendMessage(new SensorMessage(status()));
			battery.decreaseCharge(SENSOR_LIFE_DISCHARGE + SEND_MESSAGE_DISCHARGE);
		} catch (NoChargeException ex) {
			controlSystem.sendMessage(new SensorMessage(status()));
			sendShutdownMessage();
		}
	}

	/**
	 * Waits 100ms
	 */
	private void waitToSendMessage() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			stopSensors();
			isOn = false;
		}
	}

	@Override
	public void start() {
		super.start();
		logger.log("[" + getName() + "] starting robot sensors...");
		initializeMonitors();
		logger.log("[" + getName() + "] robot sensors initialized.");
	}

	private void sendShutdownMessage() {
		controlSystem.receiveMessage(new Message(Message.SHUTDOWN,
				Message.SHUTDOWN_ROBOT, -1));
	}

	private void stopSensors() {
		for (SensorMonitor sensor : sensors) {
			sensor.interrupt();
			sensor.updateStatus();
		}
		sendSensorStatus();
	}

	private void addMonitors(Monitorable[] components) {
		for (Monitorable robotComponent : components) {
			addComponent(robotComponent);
		}
	}

	private void addComponent(Monitorable robotComponent) {
		sensors.add(new SensorMonitor(robotComponent));
	}

	/**
	 * Initialise the monitor of the status of all the components.
	 */
	private void initializeMonitors() {
		for (SensorMonitor sensor : sensors) {
			logger.log("[" + getName() + "] initializing sensor: " + sensor);
			sensor.start();
			logger.log("[" + getName() + "] " + sensor + " initialized.");
		}
	}

	/**
	 * Gets the status of all the components.
	 * @return The status of all the components.
	 */
	private HashMap<String, ComponentStatus> status() {
		HashMap<String, ComponentStatus> allComponentStatus = new HashMap<String, ComponentStatus>();
		for (SensorMonitor sensor : sensors) {
			ComponentStatus componentStatus = sensor.componentStatus();
			if (componentStatus != null) {
				allComponentStatus.put(componentStatus.componentName(),
						componentStatus);
			}
		}
		return allComponentStatus;
	}
}
