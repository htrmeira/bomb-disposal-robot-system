package ac.uk.coventry.rtes.robot.sensor;

import java.util.HashMap;
import java.util.Set;

/**
 * This class represents the message comming from the robot platform with the
 * information about all the sensors of all components.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class SensorMessage {
	private final HashMap<String, ComponentStatus> componentStatus;
	
	public SensorMessage(HashMap<String, ComponentStatus> componentStatus) {
		this.componentStatus = componentStatus;
	}
	
	public ComponentStatus componentStatusByName(String componentName) {
		return componentStatus.get(componentName);
	}
	
	public Set<String> componentNames() {
		return componentStatus.keySet();
	}

}
