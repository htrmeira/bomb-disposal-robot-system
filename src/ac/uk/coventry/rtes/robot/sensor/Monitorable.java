package ac.uk.coventry.rtes.robot.sensor;

/**
 * This interface to represent components that has a sensor and can be
 * monitored.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public interface Monitorable {
	
	/**
	 * The class who implements this method should provide the status of the
	 * component by this method.
	 * 
	 * @return The current status of the component.
	 */
	public ComponentStatus currentStatus();
}
