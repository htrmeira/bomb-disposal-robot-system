package ac.uk.coventry.rtes.robot.sensor;

/**
 * This class represents the monitor of one sensor of one component. Each
 * monitorable component has on SensorMonitor thread.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
class SensorMonitor extends Thread {
	private final Monitorable robotComponent;
	
	private boolean isOn = true;
	
	private ComponentStatus status;
	
	public SensorMonitor(Monitorable robotComponent) {
		super(robotComponent.currentStatus().componentName());
		this.robotComponent = robotComponent;
	}
	
	@Override
	public void run() {
		while(isOn) {
			updateStatus();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				isOn = false;
			}
		}
	}

	public void updateStatus() {
		/*
		 * This update of reference does not need to be locked with a read-write
		 * lock.
		 */
		status = robotComponent.currentStatus();
	}
	
	public ComponentStatus componentStatus() {
		return status;
	}

}
