package ac.uk.coventry.rtes.robot;

import ac.uk.coventry.rtes.comunication.Message;
import ac.uk.coventry.rtes.log.Logger;
import ac.uk.coventry.rtes.robot.battery.Battery;

import static ac.uk.coventry.rtes.comunication.Message.*;

/**
 * This class represents the arm of the robot.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 *
 */
public class RobotArm extends RobotComponent {
	public static final String ARM = "RobotArm";
	
	private static final int MOVIMENT_TIME = 300;
	private static final int PLUS_ONE = 1;
	private static final int MINUS_ONE = -1;
	
	private static final int MAX_TOP = 100;
	private static final int MAX_BOTTOM = 10;
	private static final int MAX_RIGHT = 360;
	private static final int MAX_LEFT = 0;
	private static final int MIN_BACK = 0;
	private static final int MAX_FRONT = 100;

	private int currentVerticalLocalization = 10;
	private int currentHorizontalLocalization = 180;
	private int currentDistanceLocalization = 0;

	public RobotArm(Battery battery, Logger logger) {
		super(ARM, battery, logger);
	}

	@Override
	protected void executeComponentCommand(Message message) {
		switch(message.action()) {
		case MOVE_ARM_UP:
			executeUpMove(message.value());
			break;
		case MOVE_ARM_DOWN:
			executeDownMove(message.value());
			break;
		case MOVE_ARM_LEFT:
			executeLeftMove(message.value());
			break;
		case MOVE_ARM_RIGHT:
			executeRightMove(message.value());
			break;
		case MOVE_ARM_FRONT:
			executeFrontMove(message.value());
			break;
		case MOVE_ARM_BACK:
			executeBackMove(message.value());
			break;
		default:
			break;
		}
	}

	private void executeUpMove(int absValue) {
		executeVerticalMove(absValue, PLUS_ONE);
	}

	private void executeDownMove(int absValue) {
		executeVerticalMove(absValue, MINUS_ONE);
	}

	private void executeLeftMove(int absValue) {
		executeHorizontalMove(absValue, MINUS_ONE);
	}

	private void executeRightMove(int absValue) {
		executeHorizontalMove(absValue, PLUS_ONE);
	}

	private void executeFrontMove(int absValue) {
		executeDistanceMove(absValue, PLUS_ONE);
	}

	private void executeBackMove(int absValue) {
		executeDistanceMove(absValue, MINUS_ONE);
	}

	private void executeVerticalMove(int absValue, int stepsValue) {
		for (int i = 0; i < absValue; i++) {
			if(MAX_BOTTOM <= currentVerticalLocalization + stepsValue && 
					currentVerticalLocalization + stepsValue <= MAX_TOP &&
					batteryHasCharge()) {
				currentVerticalLocalization += stepsValue;
				decreaseBatteryCharge(Math.abs(stepsValue));
				simulateMovingTime(MOVIMENT_TIME);
			} else {
				return;
			}
			
		}
	}

	private void executeHorizontalMove(int absValue, int stepsValue) {
		for (int i = 0; i < absValue; i++) {
			if(MAX_LEFT <= currentHorizontalLocalization + stepsValue && 
					currentHorizontalLocalization + stepsValue <= MAX_RIGHT &&
					batteryHasCharge()) {
				currentHorizontalLocalization += stepsValue;
				decreaseBatteryCharge(Math.abs(stepsValue));
				simulateMovingTime(MOVIMENT_TIME);
			} else {
				return;
			}
		}
	}

	private void executeDistanceMove(int absValue, int stepsValue) {
		for (int i = 0; i < absValue; i++) {
			if(MIN_BACK <= currentDistanceLocalization + stepsValue && 
					currentDistanceLocalization + stepsValue <= MAX_FRONT && 
					batteryHasCharge()) {
				currentDistanceLocalization += stepsValue;
				decreaseBatteryCharge(Math.abs(stepsValue));
				simulateMovingTime(MOVIMENT_TIME);
			} else {
				return;
			}
		}
	}

	@Override
	protected String componentInfo() {
		return String.format("vertical=%d, horizontal=%d, distance=%d", 
				currentVerticalLocalization, currentHorizontalLocalization, currentDistanceLocalization);
	}
}
