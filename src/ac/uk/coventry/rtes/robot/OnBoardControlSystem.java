package ac.uk.coventry.rtes.robot;

import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;

import ac.uk.coventry.rtes.comunication.CommunicationSystem;
import ac.uk.coventry.rtes.comunication.Message;
import ac.uk.coventry.rtes.log.Logger;
import ac.uk.coventry.rtes.robot.battery.Battery;
import ac.uk.coventry.rtes.robot.battery.NoChargeException;
import ac.uk.coventry.rtes.robot.sensor.ComponentStatus;
import ac.uk.coventry.rtes.robot.sensor.Monitorable;
import ac.uk.coventry.rtes.robot.sensor.RobotSensorsMonitor;
import ac.uk.coventry.rtes.robot.sensor.SensorMessage;

import static ac.uk.coventry.rtes.comunication.Message.*;

/**
 * 
 * This class is the main class in the robot platform, it represents the control
 * system of the robot and is responsible to receive the messages and forward it
 * to the right component, this class is also responsible the forward messages
 * back to the controller the start and stop the components of the robot.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class OnBoardControlSystem extends Thread implements Monitorable {
	public final static String ON_BOARD_CONTROL_SYSTEM = "OnBoardControlSystem";
	private final static double RECEIVE_MESSAGE_DISCHARGE = 2;
	private final static int BUFFER_SIZE = 5;

	private final Logger logger = new Logger("robot");
	private final ArrayBlockingQueue<Message> receveidMessages = new ArrayBlockingQueue<Message>(
			BUFFER_SIZE);
	private final Battery battery = new Battery();
	private final RobotComponent robotArm = new RobotArm(battery, logger);
	private final RobotComponent robotHand = new RobotHand(battery, logger);

	private final RobotSensorsMonitor robotSensors;
	private final CommunicationSystem communicationSystem;
	private Date lastExecuted;
	private boolean isOn = true;

	public OnBoardControlSystem(CommunicationSystem communicationSystem) {
		super(ON_BOARD_CONTROL_SYSTEM);
		this.communicationSystem = communicationSystem;
		communicationSystem.setRobotControlSystem(this);
		robotSensors = new RobotSensorsMonitor(this, battery, logger, robotArm,
				robotHand);
	}

	@Override
	public void start() {
		super.start();
		logger.log("[" + getName() + "] starting robot system...");
		initializeComponents();
		logger.log("[" + getName() + "] robot system initialized.");
	}

	/**
	 * Initialize all the robots components.
	 */
	private void initializeComponents() {
		logger.start();
		robotArm.start();
		robotHand.start();
		robotSensors.start();
	}

	@Override
	public void run() {
		Message message;
		try {
			while (isOn && battery.hasCharge()) {
				/*
				 * wait until a new message exists.
				 */
				message = receveidMessages.take();
				lastExecuted = new Date();
				logger.log("[OnBoardControlSystem] message received: "
						+ message);
				if (isShutdownMessage(message)) {
					break;
				}
				// delivers the message to the right component.
				deliverMessage(message);
			}
			shutdownSystem();
		} catch (InterruptedException e) {
			shutdownSystem();
		}
	}

	/**
	 * Deliver the message to the right component.
	 * 
	 * @param message The message to be delivered.
	 */
	private void deliverMessage(Message message) {
		RobotComponent robotComponent = messageDestination(message.component());
		if (robotComponent != null) {
			robotComponent.receiveMessage(message);
		}
	}

	/**
	 * Verifies if the message is a shutdown message. 
	 * 
	 * @param message the message to be verified.
	 * @return true if it is a shutdown message, false otherwise.
	 */
	private boolean isShutdownMessage(Message message) {
		return Message.SHUTDOWN == message.component();
	}

	/**
	 * Shuts down the robot and all his components.
	 */
	private void shutdownSystem() {
		logger.log("[" + getName() + "] shutting down the robot system...");
		robotArm.interrupt();
		robotHand.interrupt();
		isOn = false;
		while (robotArm.isAlive() && robotHand.isAlive() && isAlive()) {
		}
		robotSensors.interrupt();
		logger.log("[" + getName() + "] robot system is offline.");
		logger.interrupt();
	}

	/**
	 * Provides the component that the message must be delivered.
	 * 
	 * @param component
	 *            The flag in byte of the component.
	 * @return The component that the message should be deliverd, or null if the
	 *         component does not exists.
	 */
	private RobotComponent messageDestination(byte component) {
		switch (component) {
		case ARM:
			return robotArm;
		case HAND:
			return robotHand;
		default:
			return null;
		}
	}

	/**
	 * Sends the message back to the controller. This act will decrease battery
	 * charge and a log will be stored in the file.
	 * 
	 * @param sensorMessage
	 *            The message to be sent back, note that only sensor message is
	 *            allowed.
	 */
	public void sendMessage(SensorMessage sensorMessage) {
		if (communicationSystem != null) {
			communicationSystem.receiveSensorMessage(sensorMessage);
		} else {
			logger.log("["+ getName()+ "] trying to send a message without communication system: "
					+ sensorMessage);
		}
	}

	/**
	 * Returns if the on board control system is on or off.
	 * 
	 * @return true if the on board control system is on, false if it is off.
	 */
	public boolean isOn() {
		return isOn;
	}

	/**
	 * This method receives the message. This act of receive a message implies
	 * that battery will be discharged and the message will be logged.
	 * 
	 * @param message The message to be executed.
	 */
	public void receiveMessage(Message message) {
		logger.log("[" + getName() + "] enqueueing message: " + message);
		try {
			battery.decreaseCharge(RECEIVE_MESSAGE_DISCHARGE);
		} catch (NoChargeException ex) {
			logger.log("[" + getName() + "] no charge in battery.");
			interrupt();
		}
		putMessageInBuffer(message);
	}

	/**
	 * Receives the message and tries to put in the buffer, if the buffer is
	 * full it will not wait the buffer to be empty.
	 * 
	 * @param message The message to be putted in the buffer.
	 */
	private void putMessageInBuffer(Message message) {
		receveidMessages.offer(message);
	}

	@Override
	public ComponentStatus currentStatus() {
		byte status = isOn() ? ComponentStatus.ON : ComponentStatus.OFF;
		return new ComponentStatus(lastExecuted, new Date(), status,
				"messageBuffer=" + receveidMessages.size(), getName());
	}

}
