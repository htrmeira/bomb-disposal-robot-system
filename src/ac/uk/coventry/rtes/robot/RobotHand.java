package ac.uk.coventry.rtes.robot;

import ac.uk.coventry.rtes.comunication.Message;
import ac.uk.coventry.rtes.log.Logger;
import ac.uk.coventry.rtes.robot.battery.Battery;

import static ac.uk.coventry.rtes.comunication.Message.*;

/**
 * This class represents the hand of the robot.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 *
 */
public class RobotHand extends RobotComponent {
	public final static String HAND = "RobotHand";
	
	private static final int MOVIMENT_TIME = 300;
	private static final int PLUS_ONE = 1;
	private static final int MINUS_ONE = -1;
	private static final int MAX_PRESSURE = 100;
	private static final int MIN_PRESSURE = 0;
	
	private int currentPressure = 0;
	
	public RobotHand(Battery battery, Logger logger) {
		super(HAND, battery, logger);
	}

	@Override
	protected void executeComponentCommand(Message message) {
		switch (message.action()) {
		case OPEN_HAND:
			executeOpenHandMove(message.value());
			break;
		case CLOSE_HAND:
			executeCloseHandMove(message.value());
			break;
		default:
			break;
		}
	}

	private void executeOpenHandMove(int absValue) {
		executeHandMove(absValue, MINUS_ONE);
	}

	private void executeCloseHandMove(int absValue) {
		executeHandMove(absValue, PLUS_ONE);
	}

	private void executeHandMove(int absValue, int stepsValue) {
		for (int i = 0; i < absValue; i++) {
			if(MIN_PRESSURE <= currentPressure + stepsValue && 
					currentPressure + stepsValue <= MAX_PRESSURE && 
					batteryHasCharge()) {
				currentPressure += stepsValue;
				decreaseBatteryCharge(Math.abs(stepsValue));
				simulateMovingTime(MOVIMENT_TIME);
			} else {
				return;
			}
		}
	}

	@Override
	protected String componentInfo() {
		return String.format("hand-pressure=%d", currentPressure);
	}
}
