package ac.uk.coventry.rtes.controler;

public class WrongCommandFormationException extends RuntimeException {
	private static final long serialVersionUID = -395951890713213191L;

	public WrongCommandFormationException(String message) {
		super(message);
	}
}
