package ac.uk.coventry.rtes.controler;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import ac.uk.coventry.rtes.comunication.Response;
import ac.uk.coventry.rtes.robot.OnBoardControlSystem;
import ac.uk.coventry.rtes.robot.RobotArm;
import ac.uk.coventry.rtes.robot.RobotHand;
import ac.uk.coventry.rtes.robot.battery.Battery;
import ac.uk.coventry.rtes.robot.sensor.ComponentStatus;
import ac.uk.coventry.rtes.robot.sensor.SensorMessage;

import static ac.uk.coventry.rtes.controler.StringFormater.*;
import static ac.uk.coventry.rtes.robot.sensor.ComponentStatus.*;

/**
 * This class is responsible to format the status information to print in the
 * screen.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class Display {
	private final static int TOTAL_CHARS_PER_LINE = 120;
	private final static int NAME_CELL_SIZE = 22;
	private final static int DATE_CELL_SIZE = 19;
	private static final int STATUS_CELL_SIZE = 9;
	private static final int INFO_CELL_SIZE = TOTAL_CHARS_PER_LINE - NAME_CELL_SIZE - 
			(2 * DATE_CELL_SIZE) - STATUS_CELL_SIZE - 5;
	private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	private final Lock writeLock;
	private final Lock readLock;
	
	private final String NEW_LINE = "\n";
	private SensorMessage sensorInfoStored;
	
	public Display() {
		writeLock = readWriteLock.writeLock();
		readLock = readWriteLock.readLock();
	}

	/**
	 * The information can not updated while it is being read. This control is
	 * being made with a write lock of a Read-Write lock.
	 * 
	 * @param sensorInfoStored
	 *            The new data about the sensors.
	 */
	public void updateSensorInfo(SensorMessage sensorInfoStored) {
		try {
			writeLock.lock();
			this.sensorInfoStored = sensorInfoStored;
		} finally {
			writeLock.unlock();
		}
	}

	/**
	 * This method prepare a message to be show in the screen saying that there
	 * is no status information to be shown.
	 * 
	 * @return A response with the right message content saying there is no
	 *         information to be shown.
	 */
	private Response nothingToShow() {
		String info = lineDivisor(TOTAL_CHARS_PER_LINE) + NEW_LINE;
		info += VERTICAL_LINE_DIVISOR + alignCentralizeInCell(TOTAL_CHARS_PER_LINE, "Nothing to show") 
				+ VERTICAL_LINE_DIVISOR + NEW_LINE;
		info += lineDivisor(TOTAL_CHARS_PER_LINE) + NEW_LINE;
		return new Response(Response.ERROR_TO_SHOW, info);
	}

	/**
	 * Format the information about the status of one component to be shown.
	 * 
	 * @param componentStatus The component that the information will be shown.
	 * 
	 * @return The formated information.
	 */
	private String printComponentInfo(ComponentStatus componentStatus) {
		return VERTICAL_LINE_DIVISOR + alignLeftInCell(NAME_CELL_SIZE, componentStatus.componentName()) +
		VERTICAL_LINE_DIVISOR + alignCentralizeInCell(DATE_CELL_SIZE, formatDate(componentStatus.lastExecuted())) + 
		VERTICAL_LINE_DIVISOR + alignCentralizeInCell(DATE_CELL_SIZE, formatDate(componentStatus.lastMonitored())) +
		VERTICAL_LINE_DIVISOR + alignCentralizeInCell(STATUS_CELL_SIZE, formatStatus(componentStatus.currentStatus())) +
		VERTICAL_LINE_DIVISOR + alignLeftInCell(INFO_CELL_SIZE, componentStatus.componentInformation()) + 
		VERTICAL_LINE_DIVISOR;
	}

	/**
	 * This methods collect the information about the all components.
	 * 
	 * @return All the status information formated.
	 */
	private Response infoToShow() {
		String info = lineDivisor(TOTAL_CHARS_PER_LINE) + NEW_LINE;
		info += printHeader() + NEW_LINE;
		info += lineDivisor(TOTAL_CHARS_PER_LINE) + NEW_LINE;
		info += printComponentInfo(sensorInfoStored
				.componentStatusByName(OnBoardControlSystem.ON_BOARD_CONTROL_SYSTEM))
				+ NEW_LINE;
		info += lineDivisor(TOTAL_CHARS_PER_LINE) + NEW_LINE;
		info += printComponentInfo(sensorInfoStored.componentStatusByName(RobotArm.ARM)) + NEW_LINE;
		info += lineDivisor(TOTAL_CHARS_PER_LINE) + NEW_LINE;
		info += printComponentInfo(sensorInfoStored.componentStatusByName(RobotHand.HAND)) + NEW_LINE;
		info += lineDivisor(TOTAL_CHARS_PER_LINE) + NEW_LINE;
		info += printComponentInfo(sensorInfoStored.componentStatusByName(Battery.BATTERY)) + NEW_LINE;
		info += lineDivisor(TOTAL_CHARS_PER_LINE) + NEW_LINE;
		return new Response(Response.OK_TO_SHOW, info);
	}

	/**
	 * Provides a formated header with the name of field that will show the
	 * status information of the components.
	 * 
	 * @return The formated header information.
	 */
	private String printHeader() {
		return VERTICAL_LINE_DIVISOR + alignCentralizeInCell(NAME_CELL_SIZE, "NAME") + 
					VERTICAL_LINE_DIVISOR + alignCentralizeInCell(DATE_CELL_SIZE, "LAST EXECUTED") +
					VERTICAL_LINE_DIVISOR + alignCentralizeInCell(DATE_CELL_SIZE, "LAST MONITORED") + 
					VERTICAL_LINE_DIVISOR + alignCentralizeInCell(STATUS_CELL_SIZE, "STATUS") + 
					VERTICAL_LINE_DIVISOR + alignCentralizeInCell(INFO_CELL_SIZE, "INFO") + 
					VERTICAL_LINE_DIVISOR;
	}

	/**
	 * This method provides the formated information about all the components or
	 * a message saying that there is nothing to show.
	 * 
	 * @return The formated status information or a message saying that there is
	 *         nothing to show.
	 */
	public Response info() {
		try {
			readLock.lock();
			if(sensorInfoStored == null) {
				return nothingToShow();
			} else {
				return infoToShow();
			}
		} finally {
			readLock.unlock();
		}
	}
}
