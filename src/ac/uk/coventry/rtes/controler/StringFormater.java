package ac.uk.coventry.rtes.controler;

/**
 * This class is used to format the strings to be printed to the user.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 *
 */
public class StringFormater {
	public final static String HORIZONTAL_LINE_DIVISOR = "-";
	public final static String VERTICAL_LINE_DIVISOR = "|";
	private final static String BLANK_SPACE = " ";

	public static String alignCentralizeInCell(int cellMaxSize, String content) {
		String cell = new String();
		int blankSize = ((cellMaxSize - content.length()) / 2);
		for (int i = 0; i < blankSize; i++) {
			cell += BLANK_SPACE;
		}
		cell += content;
		for (int i = 0; i < blankSize; i++) {
			cell += BLANK_SPACE;
		}
		for (int i = cell.length(); i < cellMaxSize; i++) {
			cell += BLANK_SPACE;
		}
		return cell;
	}

	public static String alignLeftInCell(int cellMaxSize, String content) {
		String hostnameResult = BLANK_SPACE + content;

		for (int i = hostnameResult.length(); i < cellMaxSize; i++) {
			hostnameResult += BLANK_SPACE;
		}

		return hostnameResult;
	}

	public static String formatTime(int sizeMax, long upTime) {
		String result = new String();
		result += formatTime(upTime);
		return alignCentralizeInCell(sizeMax, result);
	}
	
	public static String formatFileNum(int resumeCelSizeMax, int numberOfFiles) {
		String result = String.valueOf(numberOfFiles);
		return alignCentralizeInCell(resumeCelSizeMax, result);
	}

	public static String formatStatus(int cellSize, boolean bool) {
		if (bool) {
			return alignCentralizeInCell(cellSize, "up");
		}
		return alignCentralizeInCell(cellSize, "down");
	}
	
	private static String formatTime(long elapsedTime) {
		String format = String.format("%%0%dd", 2);
		elapsedTime = elapsedTime / 1000;
		String seconds = String.format(format, elapsedTime % 60);
		String minutes = String.format(format, (elapsedTime % 3600) / 60);
		String hours = String.format(format, elapsedTime / 3600);
		String time = hours + "h:" + minutes + "m:" + seconds + "s";
		return time;
	}

	public static String lineDivisor(int size) {
		String lineDivisor = new String();
		for (int i = 0; i <= size; i++) {
			lineDivisor += HORIZONTAL_LINE_DIVISOR;
		}
		return lineDivisor;
	}
}