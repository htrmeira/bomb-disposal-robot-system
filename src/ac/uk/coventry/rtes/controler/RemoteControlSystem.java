package ac.uk.coventry.rtes.controler;

import java.util.concurrent.ArrayBlockingQueue;

import ac.uk.coventry.rtes.comunication.CommunicationSystem;
import ac.uk.coventry.rtes.comunication.Message;
import ac.uk.coventry.rtes.comunication.Response;
import ac.uk.coventry.rtes.log.Logger;
import ac.uk.coventry.rtes.robot.OnBoardControlSystem;
import ac.uk.coventry.rtes.robot.sensor.ComponentStatus;
import ac.uk.coventry.rtes.robot.sensor.SensorMessage;

/**
 * This class represents the remote control that the user has access.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 *
 */
public class RemoteControlSystem extends Thread {
	private static final String REMOTE_CONTROL_SYSTEM = "RemoteControlSystem";
	private final Logger logger = new Logger("controller");
	private final ArrayBlockingQueue<Message> messageBuffer = new ArrayBlockingQueue<Message>(5);
	private final Display display = new Display();
	private final ToMessageConversor messageGenerator = new ToMessageConversor();
	
	private final CommunicationSystem communicationSystem;
	private SensorMessage sensorMessage;
	
	private boolean isOn = true;
	
	/**
	 * The remote control has a communication system, that is used to send the
	 * messages to the on board control system.
	 * 
	 * @param communicationSystem The communication system.
	 */
	public RemoteControlSystem(CommunicationSystem communicationSystem) {
		super(REMOTE_CONTROL_SYSTEM);
		this.communicationSystem = communicationSystem;
		communicationSystem.setRemoteControlSystem(this);
	}
	
	@Override
	public void run() {
		while(isOn) {
			try {
				/*
				 * this is the waiting point, this thread will always wait unit
				 * a message is available in buffer.
				 */
				sendMessage(messageBuffer.take());
			} catch (InterruptedException e) {
				isOn = false;
				logger.interrupt();
			}
		}
	}
	
	@Override
	public synchronized void start() {
		super.start();
		logger.start();
	}

	/**
	 * Updates the current status information of the components of the robot
	 * platform.
	 * 
	 * @param sensorMessage The message with the new component status.
	 */
	public void storeSensorMessage(SensorMessage sensorMessage) {
		this.sensorMessage = sensorMessage;
		display.updateSensorInfo(sensorMessage);
	}
	
	private boolean isDisplayCommand(Message message) {
		return message.component() == Message.SHOW_INFO;
	}
	
	private boolean isShutdownControllerCommand(Message message) {
		return message.action() == Message.SHUTDOWN_CONTROLLER;
	}

	/**
	 * Sends a message to the on board control system.
	 * 
	 * @param message The message to send.
	 */
	private void sendMessage(Message message) {
		communicationSystem.send(message);
	}
	
	private boolean robotIsOn() {
		return sensorMessage.componentStatusByName(
				OnBoardControlSystem.ON_BOARD_CONTROL_SYSTEM).currentStatus() == ComponentStatus.ON;
	}

	/**
	 * Receives a command to be converted to a message be send.
	 * 
	 * @param commandArray
	 *            the command to be executed.
	 * @return The response of the non asynchronous components.
	 * @throws WrongCommandFormationException
	 *             If there is some problem with the command formation.
	 */
	public Response receiveCommand(String[] commandArray) throws WrongCommandFormationException {
		Message message = messageGenerator.createMessage(commandArray);
		if(isDisplayCommand(message)) {
			return display.info();
		} else if(isShutdownControllerCommand(message)) {
			return tryToShutdownController();
		} else {
			return putMessageInBuffer(message);
		}
	}

	private Response putMessageInBuffer(Message message) {
		if(!robotIsOn()) {
			logger.log("[" + getName() + "] trying to send a message with robot off");
			return new Response(Response.ERROR_TO_SHOW, "robot is off");
		}
		if(messageBuffer.offer(message)){
			logger.log("[" + getName() + "] enqueuing: " + message);
			return new Response(Response.OK_NOTHING_TO_SHOW, "received");
		} else {
			logger.log("[" + getName() + "] message buffer is full. Check if robot is still on");
			return new Response(Response.ERROR_TO_SHOW, getName() + " message buffer is full. Check if robot is still on");
		}
	}

	/**
	 * Try to shutdown the controller, but only if the robot platform is already off.
	 * @return
	 */
	private Response tryToShutdownController() {
		if(robotIsOn()) {
			logger.log("[" + getName() + "] trying to shutdown the controller with robot still on");
			return new Response(Response.ERROR_I_AM_ALIVE, "the robot is still on");
		} else {
			logger.log("[" + getName() + "] shutting down the controller");
			interrupt();
			return new Response(Response.OK_SHUTDOWN_TO_SHOW, "shutting down the controller");
		}
	}
}
