package ac.uk.coventry.rtes.controler;

import java.util.Scanner;

import ac.uk.coventry.rtes.comunication.Response;

/**
 * This class represents the interface between the user and the controller. It
 * is responsible to capture the commands from the command line and print the
 * informations and reponses.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class UserInterface extends Thread {
	private boolean robotOn = true;
	private final RemoteControlSystem remoteControlSystem;

	/**
	 * A user interface must have a controller.
	 * 
	 * @param remoteControlSystem
	 *            The controller.
	 */
	public UserInterface(RemoteControlSystem remoteControlSystem) {
		this.remoteControlSystem = remoteControlSystem;
	}

	@Override
	public void run() {
		@SuppressWarnings("resource")
		Scanner sc = new Scanner(System.in);
		while (robotOn) {
			try {
				System.out.print("user@robot: ");
				String command = sc.nextLine(); // Reads the command from the
												// command line.
				String[] commandArray = extractCommand(command);

				Response response = this.remoteControlSystem
						.receiveCommand(commandArray);
				System.out.print(checkResponse(response));
			} catch (Throwable ex) {
				System.out.println(ex.getMessage());
			}
		}
	}

	private String checkResponse(Response response) {
		if (response.response() == Response.OK_SHUTDOWN_TO_SHOW) {
			robotOn = false;
		}
		return response.toString();
	}

	/**
	 * Splits the String command.
	 * 
	 * @param command
	 *            The String command.
	 * @return An array with the splitted command.
	 * @throws WrongCommandFormationException
	 *             If the command is not well formed.
	 */
	private String[] extractCommand(String command)
			throws WrongCommandFormationException {
		String[] commandMembers = command.split(" ");
		checkCommand(commandMembers);
		return commandMembers;
	}

	/**
	 * Check if the command is well formed.
	 * 
	 * @param commandMembers The command.
	 * @throws WrongCommandFormationException If the command is not well formed.
	 */
	private void checkCommand(String[] commandMembers)
			throws WrongCommandFormationException {
		if (commandMembers.length > 3) {
			throw new WrongCommandFormationException(
					"should be: [arm | hand] [action] [value] | show | shutdown [robot | controller]");
		}
		try {
			if (commandMembers.length > 2) {
				Integer.valueOf(commandMembers[2]);
			}
		} catch (NumberFormatException ex) {
			throw new WrongCommandFormationException(
					"should be: [arm | hand] [action] [value] | show | shutdown [robot | controller]");
		}
	}
}
