package ac.uk.coventry.rtes.controler;

import ac.uk.coventry.rtes.comunication.Message;

/**
 * This class is responsible convert a command to a message to be send to the on
 * board control system.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class ToMessageConversor {
	private static final String SHOW = "show";
	private static final String SHUTDOWN = "shutdown";
	
	private static final String ARM = "arm";
	private static final String HAND = "hand";
	private static final String ROBOT = "robot";
	private static final String CONTROLLER = "controller";
	
	private static final String UP = "up";
	private static final String DOWN = "down";
	private static final String LEFT = "left";
	private static final String RIGHT = "right";
	private static final String FRONT = "front";
	private static final String BACK = "back";
	private static final String OPEN = "open";
	private static final String CLOSE = "close";
	
	/**
	 * Extracts the arm action from the command.
	 * 
	 * @param command
	 *            The command.
	 * @return The arm action.
	 * @throws WrongCommandFormationException
	 *             If a action specified action does not exists.
	 */
	private byte extractArmAction(String[] command) throws WrongCommandFormationException {
		String action = command[1].toLowerCase().trim();
		if(BACK.equals(action)) {
			return Message.MOVE_ARM_BACK;
		} else if(DOWN.equals(action)) {
			return Message.MOVE_ARM_DOWN;
		} else if(FRONT.equals(action)) {
			return Message.MOVE_ARM_FRONT;
		} else if(LEFT.equals(action)) {
			return Message.MOVE_ARM_LEFT;
		} else if(RIGHT.equals(action)) {
			return Message.MOVE_ARM_RIGHT;
		} else if(UP.equals(action)) {
			return Message.MOVE_ARM_UP;
		} else {
			throw new WrongCommandFormationException("should be: [up] [down] [left] [right] [front] [back]...");
		}
	}
	
	/**
	 * Extracts the hand action from the command.
	 * 
	 * @param command
	 *            The command.
	 * @return The hand action.
	 * @throws WrongCommandFormationException
	 *             If a action specified action does not exists.
	 */
	private byte extractHandAction(String[] command) throws WrongCommandFormationException {
		String action = command[1].toLowerCase().trim();
		if(OPEN.equals(action)) {
			return Message.OPEN_HAND;
		} else if(CLOSE.equals(action)) {
			return Message.CLOSE_HAND;
		} else {
			throw new WrongCommandFormationException("should be: [open] [close] ...");
		}
	}

	/**
	 * Extracts the shutdown action from the command.
	 * 
	 * @param command
	 *            The command.
	 * @return The shutdown action.
	 * @throws WrongCommandFormationException
	 *             If a action was not specified or the action does not exists.
	 */
	private byte extractShutdownAction(String[] command) throws WrongCommandFormationException {
		String action;
		try {
			action = command[1].toLowerCase().trim();
		} catch (ArrayIndexOutOfBoundsException ex) {
			throw new WrongCommandFormationException("should have a second argument");
		}
		if(ROBOT.equals(action)) {
			return Message.SHUTDOWN_ROBOT;
		} else if(CONTROLLER.equals(action)) {
			return Message.SHUTDOWN_CONTROLLER;
		} else {
			throw new WrongCommandFormationException("should be: [robot] [controller] ...");
		}
	}

	/**
	 * Extracts the integer value from the command.
	 *  
	 * @param command The command.
	 * @return The value.
	 */
	private int extractValue(String[] command) {
		if(command.length < 3) {
			return -1;
		}
		String valueString = command[2].toLowerCase().trim();
		return Integer.valueOf(valueString);
	}

	/**
	 * Extract the action of the command.
	 * 
	 * @param component
	 *            The component that the action is related to.
	 * @param command
	 *            The command.
	 * @return The action of the specified component.
	 * @throws WrongCommandFormationException
	 *             If the action does not exists or is not applicable to that
	 *             component.
	 */
	private byte action(byte component, String[] command)throws WrongCommandFormationException {
		switch(component) {
		case Message.ARM:
			return extractArmAction(command);
		case Message.HAND:
			return extractHandAction(command);
		case Message.SHUTDOWN:
			return extractShutdownAction(command);
		default:
			return -1;
		}
	}

	/**
	 * Extracts the component of a command.
	 * 
	 * @param command  The command.
	 * @return The component of the command.
	 * @throws WrongCommandFormationException
	 *             If the component specified does not exists.
	 */
	private byte extractComponent(String[] command) throws WrongCommandFormationException {
		String component = command[0].toLowerCase().trim();
		if(ARM.equals(component)) {
			return Message.ARM;
		} else if(HAND.equals(component)) {
			return Message.HAND;
		} else if (SHOW.equals(component)) {
			return Message.SHOW_INFO;
		} else if(SHUTDOWN.equals(component)) {
			return Message.SHUTDOWN;
		} else {
			throw new WrongCommandFormationException("should be: [arm] [hand] [show] [shutdown]");
		}
	}

	/**
	 * Creates a messages from a command array.
	 * 
	 * @param command the command to be converted into a message.
	 * @return The message representing the command.
	 */
	public Message createMessage(String[] command) {
		byte component = extractComponent(command);
		byte action = action(component, command);
		int value = extractValue(command);
		return new Message(component, action, value);
	}

}
