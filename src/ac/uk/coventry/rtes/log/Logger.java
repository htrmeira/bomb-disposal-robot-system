package ac.uk.coventry.rtes.log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * This class logs the information to a file.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 *
 */
public class Logger extends Thread {
	private final ArrayBlockingQueue<String> contentToLog = new ArrayBlockingQueue<String>(10);
	private final static String FIELD_SEPARATOR = "-";
	private final String filePath;

	private FileWriter fileWriter;
	private BufferedWriter writer;

	private boolean isOn = true;

	/**
	 * Path of the file where the logs will be stored.
	 * @param filePath The path of the log file.
	 */
	public Logger(String filePath) {
		this.filePath = formatFileName(filePath);
	}

	/**
	 * The name of the log file must have the date of the log creation.
	 * @param filePath the path of the log file.
	 * @return The formated name of the log file with date and time.
	 */
	public String formatFileName(String filePath) {
		return filePath + FIELD_SEPARATOR + currentTime() + ".log";
	}

	private String currentTime() {
		Calendar calendar = Calendar.getInstance();
		return calendar.get(Calendar.YEAR) + FIELD_SEPARATOR +
				(calendar.get(Calendar.MONTH) + 1) + FIELD_SEPARATOR + 
				calendar.get(Calendar.DAY_OF_MONTH) + FIELD_SEPARATOR + 
				calendar.get(Calendar.HOUR_OF_DAY) + FIELD_SEPARATOR + 
				calendar.get(Calendar.MINUTE) + FIELD_SEPARATOR + 
				calendar.get(Calendar.SECOND);
	}

	@Override
	public void run() {
		while(isOn) {
			try {
				/*
				 * write the content of the buffer and waits if there is no
				 * content to be writen
				 */
				write(contentToLog.take());
			} catch (InterruptedException e) {
				isOn = false;
				for (String iterable_element : contentToLog) {
					write(iterable_element);
				}
				stopLogger();
			}
		}
	}
	
	public void stopLogger() {
		try {
			close();
		} catch (IOException e) {}
	}
	
	@Override
	public void start() {
		super.start();
		try {
			fileWriter = createFileWriter(filePath);
			writer = new BufferedWriter(fileWriter);
		} catch (IOException e) {e.printStackTrace();}
		
	}
	
	private FileWriter createFileWriter(String filePath) throws IOException {
		File file = new File(filePath);
		if(!file.exists()) {
			file.createNewFile();
		}
		return new FileWriter(file.getAbsoluteFile());
	}

	private void close() throws IOException {
		writer.close();
		fileWriter.close();
	}
	
	/**
	 * Put content in the buffer to be write in the log file. note that the
	 * time stamp will be added to the information.
	 * 
	 * @param content The content to be stored in the file.
	 */
	public void log(String content) {
		if(isOn) {
			String time =  DateFormat.getDateTimeInstance(DateFormat.SHORT, 
					DateFormat.MEDIUM).format(new Date());
			try {
				contentToLog.put("["+ time + "] " + content);
			} catch (InterruptedException e) {}
		}
	}
	
	/**
	 * Write the content into the log file.
	 * 
	 * @param content The content to be stored.
	 */
	private void write(String content) {
		if(writer != null && isOn) {
			try {
				writer.write(content);
				writer.newLine();
				writer.flush();
			} catch (IOException e) {e.printStackTrace();}
		}
	}
}
