package ac.uk.coventry.rtes.comunication;

import ac.uk.coventry.rtes.controler.RemoteControlSystem;
import ac.uk.coventry.rtes.robot.OnBoardControlSystem;
import ac.uk.coventry.rtes.robot.sensor.SensorMessage;

/**
 * This class represents the communication in the middle. It does not has useful
 * methods it only function is to represent the communication process.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class CommunicationSystem {
	private OnBoardControlSystem robotControlSystem;
	private RemoteControlSystem remoteControl;

	/**
	 * Sets the component responsible to control the robot system remotely.
	 * 
	 * @param remoteControl The remote control.
	 */
	public void setRemoteControlSystem(RemoteControlSystem remoteControl) {
		this.remoteControl = remoteControl;
	}

	/**
	 * Sets the component responsible control the robot, this system is in the
	 * robot platform itself.
	 * 
	 * @param robotControlSystem The robot platform control system.
	 */
	public void setRobotControlSystem(OnBoardControlSystem robotControlSystem) {
		this.robotControlSystem = robotControlSystem;
	}

	/**
	 * Receives a message to be passed to the controller, this message comes
	 * from the on board control system.
	 * 
	 * @param sensorMessage The message to be received by the controller.
	 */
	public void receiveSensorMessage(SensorMessage sensorMessage) {
		remoteControl.storeSensorMessage(sensorMessage);
	}

	/**
	 * Sends a message on board control system in the robot platform.
	 * 
	 * @param message The message to be send to the on board control system.
	 */
	public void send(Message message) {
		robotControlSystem.receiveMessage(message);
	}
}
