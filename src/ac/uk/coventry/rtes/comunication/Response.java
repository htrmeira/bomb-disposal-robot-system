package ac.uk.coventry.rtes.comunication;

/**
 * This class represents the response that is send to the user interface. As the
 * system is based on asynchronous message passing, this response only occur
 * with actions that did not reached the any buffer queue and is the also the
 * component that encapsulates message to be shown in the user interface.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class Response {
	/*
	 * used to say that everything occurred well and there is a message to the
	 * user.
	 */
	public static final byte OK_TO_SHOW = 1;
	/*
	 * used to say that everything occurred well but there is no message to show
	 * to the user.
	 */
	public static final byte OK_NOTHING_TO_SHOW = 2;
	/*
	 * used to say that an error occurred and there is a message to show to
	 * the user.
	 */
	public static final byte ERROR_TO_SHOW = 3;
	/*
	 * used to say that some component is shutting down and there is a message
	 * to be shown to the user.
	 */
	public static final byte OK_SHUTDOWN_TO_SHOW = 4;
	// used to say that the robot system is still up.
	public static final byte ERROR_I_AM_ALIVE = 5;
	
	private final byte response;
	private final String messageToShow;

	/**
	 * A response has a message that can be showed to the user but only in case
	 * of the response type is the correct one. To more information about the
	 * type that can show the message take a look at public constants of this
	 * class.
	 * 
	 * @param response
	 *            The type of this response can be: OK_TO_SHOW,
	 *            OK_NOTHING_TO_SHOW, ERROR_TO_SHOW, OK_SHUTDOWN_TO_SHOW or
	 *            ERROR_I_AM_ALIVE.
	 * @param messageToShow The message that can be showed to the user.
	 */
	public Response(byte response, String messageToShow) {
		this.response = response;
		this.messageToShow = messageToShow;
	}

	/**
	 * Provides the response type.
	 * 
	 * @return The response type.
	 */
	public byte response() {
		return response;
	}
	
	/**
	 * Only provides the message if the response type permits.
	 */
	@Override
	public String toString() {
		switch (response) {
		case OK_TO_SHOW :
		case ERROR_TO_SHOW:
		case OK_SHUTDOWN_TO_SHOW:
		case ERROR_I_AM_ALIVE:
			return messageToShow + "\n";
		default:
			return new String();
		}
	}
}
