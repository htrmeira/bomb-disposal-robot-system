package ac.uk.coventry.rtes.comunication;

/**
 * This class represents the message that is sent across the network and is the
 * only way to communicate between controller -> on board control system. All
 * the commands are represented by this class to go to the on board control
 * system. Note that this class only represents the message in one direction,
 * coming from the controller going to the on board control system.
 * 
 * This class also has the values of the messages to represent components and 
 * action.
 * 
 * A user command could be: arm up 10
 * represented by this class it would be: ARM, MOVE_ARM_UP, 10cm.
 * 
 * @author Heitor Meira demeloh@uni.coventry.ac.uk
 * 
 */
public class Message {
	// This block represent the commands related to move the arm.
	public final static byte ARM = 10;
	public final static byte MOVE_ARM_UP = ARM + 1;
	public final static byte MOVE_ARM_DOWN = ARM + 2;
	public final static byte MOVE_ARM_LEFT = ARM + 3;
	public final static byte MOVE_ARM_RIGHT = ARM + 4;
	public final static byte MOVE_ARM_FRONT = ARM + 5;
	public final static byte MOVE_ARM_BACK = ARM + 6;

	// This block represent the commands related to move the hand.
	public final static byte HAND = 20;
	public final static byte OPEN_HAND = HAND + 1;
	public final static byte CLOSE_HAND = HAND + 2;

	/*
	 *  This block represent the commands related to show the system status in
	 *  the screen.
	 */
	public final static byte SHOW_INFO = 30;

	// This block represent the commands related to shutdown the system.
	public final static byte SHUTDOWN = 40;
	public final static byte SHUTDOWN_ROBOT = SHUTDOWN + 1;
	public final static byte SHUTDOWN_CONTROLLER = SHUTDOWN + 2;

	private final byte action;
	private final byte component;
	private final int value;

	/**
	 * A message is formed by a component (ARM, HAND, SHOW or SHUTDOWN), and the
	 * action that the component must realize, the action is based on the
	 * component. The third component of a message is the value the action must execute.
	 * 
	 * @param component The component that will execute the action.
	 * @param action The action to be executed.
	 * @param value The value that the action must execute.
	 */
	public Message(byte component, byte action, int value) {
		this.component = component;
		this.action = action;
		this.value = value;
	}

	/**
	 * Provides the component that the message is directed to.
	 * 
	 * @return The component that this messages is directed to.
	 */
	public byte component() {
		return component;
	}

	/**
	 * Provides the action that the component must execute.
	 * 
	 * @return The action that the component must execute.
	 */
	public byte action() {
		return action;
	}

	/**
	 * Provides the value of the action (e.g. arm up 10, the arm component must
	 * go up 10cm)
	 * 
	 * @return The value of the action.
	 */
	public int value() {
		return value;
	}
	
	@Override
	public String toString() {
		return String.format("component=%d, action=%d, value=%d", component(), action(), value());
	}
}
